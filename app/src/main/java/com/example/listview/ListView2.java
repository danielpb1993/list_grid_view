package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListView2 extends AppCompatActivity {
    private ListView listView;

    List<String> list = new ArrayList<>();
    ArrayAdapter<String> dataAdapter;
    CustomAdapter customAdapter = new CustomAdapter(this, items);

    Item item1 = new Item(R.drawable.puppy, "Gos", "Gosset maco", "No té preu");

    ArrayList<Item> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view2);

        listView = findViewById(R.id.listView);

        list.add("hola");
        list.add("adeu");
        list.add("ei");
        list.add("pst");
        list.add("yep");

        items.add(item1);

        dataAdapter = new ArrayAdapter<String>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                list
        );

        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ListView2.this, "Click: "+ i, Toast.LENGTH_SHORT).show();
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ListView2.this, "Long click: "+ i, Toast.LENGTH_SHORT).show();
                return true;
            }
        });


    }

    private class CustomAdapter extends BaseAdapter {
        private Context context;
        private List<Item> items;

        public CustomAdapter(Context context, List<Item> items) {
            this.context = context;
            this.items = items;
        }


        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int i) {
            return items.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View myView = getLayoutInflater().inflate(R.layout.row_data, null);
            TextView textRow = view.findViewById(R.id.textRow);
            ImageView imageRow = view.findViewById(R.id.imageRow);
            TextView textSubRow = view.findViewById(R.id.textSubRow);
            TextView textPriceRow = view.findViewById(R.id.textPriceRow);
            textRow.setText(items.get(i).getTitle());
            textSubRow.setText(items.get(i).getSubtitle());
            textPriceRow.setText(items.get(i).getPrice());
            imageRow.setImageResource(items.get(i).getImage());

            return view;

        }
    }
}